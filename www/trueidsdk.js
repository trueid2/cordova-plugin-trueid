/**
* cordova is available under *either* the terms of the modified BSD license *or* the
* MIT License (2008). See http://opensource.org/licenses/alphabetical for full text.
*
* Copyright (c) Matt Kane 2010
* Copyright (c) 2011, IBM Corporation
*/

    var exec = require("cordova/exec");
function TrueIDSDK() {}

TrueIDSDK.prototype.scan = function (successCallback, errorCallback, config) {

    if (config instanceof Array) {
    // do nothing
    } else {
        if (typeof(config) === 'object') {
            config = [ config ];
        } else {
            config = [];
        }
    }

    if (errorCallback == null) {
        errorCallback = function () {};
    }

    if (typeof errorCallback != "function") {
        console.log("failure: failure parameter not a function");
        return;
    }

    if (typeof successCallback != "function") {
        console.log("failure: success callback parameter must be a function");
        return;
    }

    exec(
        function(result) {
            successCallback(result);
        },
        function(error) {
            errorCallback(error);
        },
        'TrueIDSDK',
        'scan',
        config
    );
};

TrueIDSDK.prototype.configure = function (successCallback, errorCallback, config) {
if (config instanceof Array) {
    // do nothing
    } else {
        if (typeof(config) === 'object') {
            config = [ config ];
        } else {
            config = [];
        }
    }
    exec(
    function(result) {
        successCallback(result);
    },
    function(error) {
        errorCallback(error);
    },
    'TrueIDSDK',
    'configure',
    config
    );
};

var trueIDSDK = new TrueIDSDK();
module.exports = trueIDSDK;


// exports.coolMethod = function (arg0, success, error) {
//     exec(success, error, 'TrueIDSDK', 'coolMethod', [arg0]);
// };
// exports.configure = function (arg0, success, error) {
//     exec(success, error, 'TrueIDSDK', 'configure', [arg0]);
// };
// exports.start = function (arg0, success, error) {
//     exec(success, error, 'TrueIDSDK', 'start', [arg0]);
// };