# Trueid SDK Cordova Plugin for iOS and Android.
## NOTE: Data document included in folder doc in this Plugin##
## License

You can request for a free trial license online from TrueID of VNG.

## Installation

1. Download the cordova-plugin-trueid 
2. Install plugin 
   - cordova plugin add <path/to/plugin>  
3. Library base on AndroidX, plz turn on Android X on Config.xml of Project.
//==============================================================================//
replace tag "android" with :  <preference name="AndroidXEnabled" value="true" />
<platform name="android">
    <allow-intent href="market:*" />
    <preference name="AndroidXEnabled" value="true" />
</platform>
//==============================================================================//
4. Code sample please review folder "Samp".
   for now, it just support for Android.

*** NOTE *** 
IOS : add TrueIDSDK.framework, SwiftProtobuf.framework, ZoomAuthentication.framework to Frameworks 

## Usage 
SDK support 3 method : "configure","start","setLanguage"
Please review file com.phonegap.plugins.trueid.TrueIDSDK.java
For now "setLanguage" - support for English and Viet Nam.
by default language using default location on Phone.
//================================================================
 * Configure TrueIDSdk  : 
   
       var configInfo = {
        domain: "https://api.trueid.ai",
        domainPath: "/ekyc/v1.0",
        authDomain: "https://api.trueid.ai",
        authDomainPath: "/v1/oauth",
        appId: "3c4bda2dfbafd5a82c16eada97c8499e",
        appSecret: "Yms0u7uYPmIJ8/S6Q5cjYmED7FEo/u851WfPP0M4dpI=",
        language:"vi",          //option "vi","en"
      }

   cordova.exec(null, null, "TrueIDSDK", "configure",[configInfo]);
 
 * Start ekyc: 
    cordova.exec((CardInfo)=>{
            console.log("CardInfo", JSON.stringify(CardInfo))
           }, (error)=>{
            console.log("error", error)
           }, "TrueIDSDK", "start",[]); 
//================================================================
   
After completed eKyc Plugin return a Data Map (review CardInfo - JSON).
It will return data eKyc of User. Person, CheckingRecord,frontCardImage,backCardImage,result,decision
## NOTE: Data document included in folder doc in this Plugin##
### CardInfo 
CardInfo is object.
{
  person: {
    result: "ID",
    time_used: "GENDER",
    client_id: "date of birth",
    request_id: "NAME",
    attributes: Object - information is extracted from identity cards: id_number, name, dob (date of birth),id_origin, id_address and confidence,
    input_image_type: 3,
    liveness_strategy: 3,
    id_card_token: "token id card",
    selfie: "SELFIE IMAGE BASE 64 STRING"
  },
  records: {
    idVerification: {
      name: "NAME",
      message: "MESSAGE",
      type: RECORD_TYPE_INT,
      status: RECORD_STATUS_BOOLEAN
    },
    ocrScan: {
      name: "NAME",
      message: "MESSAGE",
      type: RECORD_TYPE_INT,
      status: RECORD_STATUS_BOOLEAN
    },
    livenessDetection, {
      name: "NAME",
      message: "MESSAGE",
      type: RECORD_TYPE_INT,
      status: RECORD_STATUS_BOOLEAN
    },
    faceComparision, {
      name: "NAME",
      message: "MESSAGE",
      type: RECORD_TYPE_INT,
      status: RECORD_STATUS_BOOLEAN
    }
  },
  "frontCardImage": "IMAGE BASE 64 STRING",
  "backCardImage": "IMAGE BASE 64 STRING",
  "result": {
    "result":1,
    "time_used":34,
    "client_id":" Id Client",
    "request_id":"Id Request"
    "kyc_result":object kyc_result
  }
}



