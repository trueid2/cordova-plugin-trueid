/********* TrueIDSDK.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <TrueIDSDK/TrueIDSDK.h>
#import <UIKit/UIKit.h>

@interface TrueIDSDK : CDVPlugin {
  // Member variables go here.
}
- (void)coolMethod:(CDVInvokedUrlCommand*)command;
- (void)configure:(CDVInvokedUrlCommand*)command;
- (void)start:(CDVInvokedUrlCommand*)command;

@end

@implementation TrueIDSDK

- (void)start:(CDVInvokedUrlCommand*)command
{
    // let callback ;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (@available(iOS 9, *)) {
            [TrueID startWithListener:^(CardInfo * _Nonnull cardInfo) {
               // if (callback) {
                    NSMutableDictionary *res = [NSMutableDictionary new];
                    // person
                    NSString * base64String;
                    if (cardInfo.person) {
                        if (cardInfo.person.selfie) {
                            NSData *imageData = UIImagePNGRepresentation(cardInfo.person.selfie);
                            base64String = [imageData base64EncodedStringWithOptions:0];
                        }
                        else {
                            base64String = @"";
                        }
                        
                        res[@"person"] = @{
                            @"idNumber": cardInfo.person.idNumber ? cardInfo.person.idNumber : @"",
                            @"gender": cardInfo.person.gender ? cardInfo.person.gender : @"",
                            @"dob": cardInfo.person.dob ? cardInfo.person.dob : @"",
                            @"fullname": cardInfo.person.fullname ? cardInfo.person.fullname : @"",
                            @"address": cardInfo.person.address ? cardInfo.person.address : @"",
                            @"doi": cardInfo.person.doi ? cardInfo.person.doi : @"",
                            @"origin": cardInfo.person.origin ? cardInfo.person.origin : @"",
                            @"dueDate": cardInfo.person.dueDate ? cardInfo.person.dueDate : @"",
                            @"selfie": base64String
                        };
                    }
                    else {
                        res[@"person"] = @{
                            @"idNumber": @"",
                            @"gender": @"",
                            @"dob": @"",
                            @"fullname": @"",
                            @"address": @"",
                            @"doi": @"",
                            @"origin": @"",
                            @"dueDate": @"",
                            @"selfie": @""
                        };
                    }
                    
                    // record
                    NSArray *recordTypes = @[
                        @"ocrScan",
                        @"livenessDetection",
                        @"faceComparision",
                        @"idVerification",
                         @"faceVerification",
                    ];
                    NSMutableDictionary *records = [NSMutableDictionary new];
                    for(NSInteger i=0; i<recordTypes.count; i++) {
                        NSString *type = recordTypes[i];
                        CheckingRecord *r = [cardInfo getRecordWithType:i];
                        if (r) {
                            records[type] = @{
                                @"name": r.name,
                                @"message": r.message,
                                @"type": [NSNumber numberWithInt:(int)r.type],
                                @"status": [NSNumber numberWithBool:r.status]
                            };
                        }
                        else {
                            records[type] = @{
                                @"name": @"",
                                @"message": @"",
                                @"type": @0,
                                @"status": @NO
                            };
                        }
                    }
                    res[@"records"] = records;
                    
                    // front image
                    if (cardInfo.frontCardImage) {
                        NSData *imageData = UIImagePNGRepresentation(cardInfo.frontCardImage);
                        base64String = [imageData base64EncodedStringWithOptions:0];
                        res[@"frontCardImage"] = base64String;
                    }
                    else {
                        res[@"frontCardImage"] = @"";
                    }
                    
                    // back image
                    if (cardInfo.backCardImage) {
                        NSData *imageData = UIImagePNGRepresentation(cardInfo.backCardImage);
                        base64String = [imageData base64EncodedStringWithOptions:0];
                        res[@"backCardImage"] = base64String;
                    }
                    else {
                        res[@"backCardImage"] = @"";
                    }

                    // result
                    if (cardInfo.result) {
                        res[@"result"] = cardInfo.result;
                    }
                    else {
                        res[@"result"] = @{};
                    }
                    
                   // NSString* payload = @[res]; //@"Success aaa";
                   // Some blocking logic...
                CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:@[res]];
                   // The sendPluginResult method is thread-safe.
                   [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//                    callback(@[res]);
//                }
            }];
        }
//        else {
//           // [self showAlert:@"trueID doesn't support iOS less than 11"];
//        }
    });
}
- (void)configure:(CDVInvokedUrlCommand*)command
{
    NSDictionary * data = [command.arguments objectAtIndex:0];;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (@available(iOS 9, *)) {
            NSString *domain = [data objectForKey:@"domain"];
            NSString *domainPath = [data objectForKey:@"domainPath"];
            NSString *authDomain = [data objectForKey:@"authDomain"];
            NSString *authDomainPath = [data objectForKey:@"authDomainPath"];
            NSString *appId = [data objectForKey:@"appId"];
            NSString *appSecret = [data objectForKey:@"appSecret"];

            ConfigInfo *configInfo;
            NSString *zoomLicenseKey = [data objectForKey:@"zoomLicenseKey"];
            if (zoomLicenseKey == nil) {
                configInfo = [[ConfigInfo alloc] initWithDomain:domain domainPath:domainPath authDomain:authDomain authDomainPath:authDomainPath appId:appId appSecret:appSecret];
            }
            else {
                NSString *zoomServerBaseURL = [data objectForKey:@"zoomServerBaseURL"];
                NSString *zoomPublicKey = [data objectForKey:@"zoomPublicKey"];
                configInfo = [[ConfigInfo alloc] initWithDomain:domain domainPath:domainPath authDomain:authDomain authDomainPath:authDomainPath appId:appId appSecret:appSecret zoomLicenseKey:zoomLicenseKey zoomServerBaseURL: zoomServerBaseURL zoomPublicKey:zoomPublicKey];
            }

            [TrueID configureWithConfigInfo:configInfo];
        }
        else {
           // [self showAlert:@"trueID doesn't support iOS less than 9"];
        }
    });
}

- (void)coolMethod:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* echo = [command.arguments objectAtIndex:0];

    if (echo != nil && [echo length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
 
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
