package com.phonegap.plugins.trueid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.content.pm.PackageManager;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PermissionHelper;

import vng.com.vn.trueid.TrueID;
import vng.com.vn.trueid.TrueIDListener;
import vng.com.vn.trueid.models.CardInfo;
import vng.com.vn.trueid.models.ConfigInfo;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
public class TrueIDSDK extends CordovaPlugin {
    public static final int REQUEST_CODE = 0x0ba7c0de;
    private static final String SCAN = "scan";
    private static final String START = "start";
    private static final String CONFIGURE = "configure";
    private static final String LOG_TAG = "TrueIDSDK";


    private JSONArray requestArgs;
    private CallbackContext callbackContext;

    /**
     * Constructor.
     */
    public TrueIDSDK() {
    }

    /**
     * Executes the request.
     * <p>
     * This method is called from the WebView thread. To do a non-trivial amount of work, use:
     * cordova.getThreadPool().execute(runnable);
     * <p>
     * To run on the UI thread, use:
     * cordova.getActivity().runOnUiThread(runnable);
     *
     * @param action          The action to execute.
     * @param args            The exec() arguments.
     * @param callbackContext The callback context used when calling back into JavaScript.
     * @return Whether the action was valid.
     * @sa https://github.com/apache/cordova-android/blob/master/framework/src/org/apache/cordova/CordovaPlugin.java
     */
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        this.callbackContext = callbackContext;
        this.requestArgs = args;
        Log.d("luantcq", "luantcq " + args.toString());
        Log.d(LOG_TAG, LOG_TAG + action);
        if (action.equals(CONFIGURE)) {
            configure(args);
        } else if (action.equals(START)) {
            start(args);
        } else if (action.equals(SCAN)) {
            start(args);
        } else {
            return false;
        }
        return true;
    }

    /**
     * Starts TrueID
     */
    public void start(final JSONArray args) {
        Log.d(LOG_TAG, "start start start");
        final CordovaPlugin that = this;
        TrueID.start(that.cordova.getActivity(), new TrueIDListener() {
            @Override
            public void completion(CardInfo cardInfo) {
                callbackContext.success(new JSONObject(cardInfo.toMap()));
            }
        });
    }

    public void configure(final JSONArray args) {
        Log.d(LOG_TAG, "configure configure configure");
        final CordovaPlugin that = this;
        try {
            JSONObject obj = args.getJSONObject(0);
            ConfigInfo info = new ConfigInfo(obj.getString("domain"), obj.getString("domainPath"),
                    obj.getString("authDomain"), obj.getString("authDomainPath"),
                    obj.getString("appId"), obj.getString("appSecret"));
            // Configure trueID
            TrueID.configure(that.cordova.getActivity(), info);
            if (obj.has("language")) {
                TrueID.setLanguage(that.cordova.getActivity(), obj.getString("language"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
