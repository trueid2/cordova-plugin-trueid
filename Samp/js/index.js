/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    document.getElementById('deviceready').classList.add('ready');

    var configInfo = {
        domain: "https://api.trueid.ai",
        domainPath: "/ekyc/v1.0",
        authDomain: "https://api.trueid.ai",
        authDomainPath: "/v1/oauth",
        appId: "176e2835e1bf213008dd3e6050b2cd57",
        appSecret: "5wQMn3UallKq2qJxujjL0OS1CK1qZPiSc2PJf8iAXfU="
      }

    cordova.exec(null, null, "TrueIDSDK", "configure",[configInfo]);
    document.getElementById("startbtn").onclick = function() {
        console.log("startbtn")
        cordova.exec((cardinfo)=>{
            console.log("cardinfo", JSON.stringify(cardinfo))
             document.getElementById('img').setAttribute('src', 'data:image/png;base64,'+cardinfo.person.selfie  );
                        document.getElementById('user_name').textContent = cardinfo.person.fullname
                        document.getElementById('user_id').textContent = cardinfo.person.idNumber
                        document.getElementById('user_gen').textContent = cardinfo.person.gender
                       document.getElementById('user_dob').textContent = cardinfo.person.dob

           }, (error)=>{
            console.log("error", error)
           }, "TrueIDSDK", "start",[]);
             
        }

        this.receivedEvent('deviceready');

}


